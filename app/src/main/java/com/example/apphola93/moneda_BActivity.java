package com.example.apphola93;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class moneda_BActivity extends AppCompatActivity {

    private EditText txtCantidad;

    private TextView txtResultado;

    private Spinner spnMoneda;

    private Button btnCalcular, btnCerrar, btnLimpiar;

    private int pos = 0; //Seleccionar la posición del Spinner

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_moneda_bactivity);
        iniciarComponentes();
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txtCantidad.getText().toString().matches("")){
                    Toast.makeText(getApplicationContext(), "Faltó capturar cantidad", Toast.LENGTH_SHORT).show();
                }
                else{
                    float cantidad = 0.0f, resultado = 0.0f;
                    cantidad = Float.parseFloat(txtCantidad.getText().toString());

                    switch (pos){
                        case 0: // Pesos a dólar mexicano
                            break;
                        case 1: // Pesos a dólar canadiense
                            break;
                        case 2: // Peso a euro
                            break;
                        case 3: //Peso a libra
                            break;
                    }
                }
            }
        });

        spnMoneda.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                pos = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void iniciarComponentes(){
        txtCantidad = (EditText) findViewById(R.id.txtCantidad);
        txtResultado = (TextView) findViewById(R.id.txtResultado);
        spnMoneda = (Spinner) findViewById(R.id.spnMonedas);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);

        //Generar el Adaptador, para llenarlo con el ArrayString
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_expandable_list_item_1, getResources().getStringArray(R.array.monedas));

        spnMoneda.setAdapter(adapter);
    }
}
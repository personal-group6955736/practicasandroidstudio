package com.example.apphola93;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class cotizacionActivity extends AppCompatActivity {
    private TextView lblNombre;
    private TextView lblFolio;
    private TextView txtValorAuto;
    private EditText txtDescripcion;
    private EditText txtPorEng;
    private RadioButton rdb12;
    private RadioButton rdb18;
    private RadioButton rdb24;
    private RadioButton rdb36;
    private TextView txtMensual;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;
    private TextView lblEnganche;
    private TextView lblPagoMensual;
    private Cotizacion cot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_cotizacion);
        lblNombre = findViewById(R.id.txtUsuario);
        lblFolio = findViewById(R.id.txtFolio);
        txtDescripcion = findViewById(R.id.txtDescripcion);
        txtValorAuto = findViewById(R.id.txtValor);
        txtPorEng = findViewById(R.id.txtPorcentaje);
        rdb12 = findViewById(R.id.rdb12);
        rdb18 = findViewById(R.id.rdb18);
        rdb24 = findViewById(R.id.rdb24);
        rdb36 = findViewById(R.id.rdb36);
        lblPagoMensual = findViewById(R.id.txtPagoMensual);
        lblEnganche = findViewById(R.id.txtPagoInicial);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        cot = new Cotizacion();

        lblFolio.setText("Folio: " + String.valueOf((cot.getFolio())));
        lblFolio.setText(String.valueOf("Folio: " + cot.generarId()));
        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("usuario");
        lblNombre.setText("Usuario: " + nombre);

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txtDescripcion.getText().toString().matches("")
                || txtValorAuto.getText().toString().matches("")
                || txtPorEng.getText().toString().matches("")){
                    Toast.makeText(cotizacionActivity.this, "Faltó capturar la información", Toast.LENGTH_SHORT).show();
                    txtDescripcion.requestFocus();
                }
                else{
                    int plazos = 0;
                    float enganche = 0.0f;
                    float pagoMensual = 0.0f;

                    if(rdb12.isChecked()){ plazos = 12; }
                    if(rdb18.isChecked()){ plazos = 18; }
                    if(rdb24.isChecked()){ plazos = 24; }
                    if(rdb36.isChecked()){ plazos = 36; }

                    cot.setDescripcion(txtDescripcion.getText().toString());
                    cot.setValorAuto(Float.parseFloat(txtValorAuto.getText().toString()));
                    cot.setPorcentajePagoInicial(Float.parseFloat(txtPorEng.getText().toString()));
                    cot.setPlazos(plazos);

                    enganche = cot.calcularPagoInicial();
                    pagoMensual = cot.calcularPagoMensual();

                    lblEnganche.setText(("Pago Inicial $" + String.valueOf(enganche)));
                    lblPagoMensual.setText(("Pago Mensual $" + String.valueOf(pagoMensual)));
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lblFolio.setText(String.valueOf("Folio: " + cot.generarId()));
                txtDescripcion.setText("");
                txtValorAuto.setText("");
                txtPorEng.setText("");
                lblPagoMensual.setText("Pago Mensual$ ");
                rdb12.setSelected(true);
                lblEnganche.setText("Pago Inicial $ ");
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
}
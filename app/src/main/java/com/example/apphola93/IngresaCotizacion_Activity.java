package com.example.apphola93;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class IngresaCotizacion_Activity extends AppCompatActivity {
    private EditText txtUsuario;

    private Button btnIngresar, btnRegresar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_ingresa_cotizacion);

        txtUsuario = (EditText) findViewById(R.id.lblUsuario);
        btnIngresar = (Button) findViewById(R.id.btnIngresar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);

        btnIngresar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(txtUsuario.getText().toString().matches("")){
                    Toast.makeText(IngresaCotizacion_Activity.this, "Faltó capturar el usuario", Toast.LENGTH_SHORT).show();
                }
                else {
                    Intent intent = new Intent(getApplicationContext(), cotizacionActivity.class);
                    intent.putExtra("usuario", txtUsuario.getText().toString());
                    startActivity(intent);
                }
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

}
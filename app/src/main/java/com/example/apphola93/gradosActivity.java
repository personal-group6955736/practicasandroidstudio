package com.example.apphola93;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class gradosActivity extends AppCompatActivity {
    private EditText txtConversion, txtCantidadGrados;

    private Button btnLimpiar, btnCerrar, btnCalcular;

    private RadioGroup radioGrupo;

    private RadioButton radioCelsius, radioFahrenheit;

    private TextView txtGrados, txtResultados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_grados);
        iniciarComponentes();
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Validar
                if(txtCantidadGrados.getText().toString().matches("")){
                    Toast.makeText(getApplicationContext(), "Faltó información", Toast.LENGTH_SHORT).show();
                }

                else if(radioFahrenheit.isChecked()){
                    double F = Float.parseFloat(txtCantidadGrados.getText().toString());
                    double C = (F - 32)* (0.55);
                    txtConversion.setText(String.valueOf(C));
                }

                else if(radioCelsius.isChecked()){
                    double C = Float.parseFloat(txtCantidadGrados.getText().toString());
                    double F = (C * 1.8) + 32;
                    txtConversion.setText(String.valueOf(F));
                }
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtConversion.setText("");
                txtCantidadGrados.setText("");
                radioCelsius.setChecked(false);
                radioFahrenheit.setChecked(false);
            }
        });
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void iniciarComponentes(){
       txtConversion = (EditText) findViewById(R.id.txtConversion);
       txtCantidadGrados = (EditText) findViewById(R.id.txtCantidadGrados);
       btnCalcular = (Button) findViewById(R.id.btnCalcular);
       btnCerrar = (Button) findViewById(R.id.btnCerrar);
       btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
       radioGrupo = (RadioGroup) findViewById(R.id.radioGrupo);
       radioCelsius = (RadioButton) findViewById(R.id.radioCelsius);
       radioFahrenheit = (RadioButton) findViewById(R.id.radioFahrenheit);
       txtGrados = (TextView) findViewById(R.id.txtGrados);
       txtResultados = (TextView) findViewById(R.id.txtResultado);
    }
}